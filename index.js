;(function($){
    $.fn.extend({
        /**
         * @version 1.2.0
         * @author 络
         * @param {*} opt 属性配置
         *      append 体现效果：弹框是否需要从元素的位置进行打开关闭
         *      content 弹框内容
         *      [样式属性]: [样式值]
         * @param {*} fn 回调方法
         */
        _Alert: function(opt,fn){ //opt 新值
            var oldValue = {
                width: 300,
                height: 160,
                background: '#41567b',
                border: '2px solid #a4bbe2',
                borderRadius: 3,
                position: 'absolute',
                padding: 10,
            }
            var option = $.extend({}, oldValue, opt||{})
            var _this = $(this).addClass('l_alter _hide')
            option.left = window.innerWidth/2-option.width/2
            option.top = window.innerHeight/2-option.height/2-10
            // 设置css
            var $box = null
            // 弹框的不同打开方式
            if(option.append){
                $box = $('<div></div>').css({
                    ...option,
                    display: 'none'
                }).html(`
                    <div class="l_content"></div>
                `).appendTo(_this)
            }else{
                $box = $(_this).css({
                    ...option,
                    display: 'none'
                }).html(`
                    <div class="l_content"></div>
                `)
            }
            // 底部
            var $footer = $('<div></div>').addClass('l_footer')
            // 按钮
            var $button = $('<div class="l_button"><div>').html('确定')
            // 给确定按钮添加事件
            $(_this).on('click', '.l_button', function(){
                $(_this)._hide()
                if(fn){fn()}
            })
            $footer.append($button)
            $($box).append($footer)
            // $(_this).append($footer)
            $(_this).__proto__._show=()=>{
                // 设置为display:none;show置为block
                // 是为了防止弹框初始化展示过渡效果
                $(_this).show().addClass('_show').removeClass('_hide')
            }
            // 关闭
            $(_this).__proto__._hide=()=>{
                $(_this).addClass('_hide').removeClass('_show')
            }
            // 打开(或是更新内容)
            $(_this).__proto__._update=(con)=>{
                $(_this).find('.l_content').html(con);
                $('.l_alter')._hide()
                setTimeout(()=>{
                    $(_this)._show()
                },200)
            }
        }
    })
})(jQuery);